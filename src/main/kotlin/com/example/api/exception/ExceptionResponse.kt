package com.example.api.exception

import java.time.LocalDateTime

data class ExceptionResponse(
    var message: String = "",
    var dateTime: LocalDateTime = LocalDateTime.now()
)