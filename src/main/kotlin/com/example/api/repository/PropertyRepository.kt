package com.example.api.repository

import com.example.api.model.Property
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PropertyRepository: JpaRepository<Property, Int> {

    fun findByDescription(description: String): Optional<Property>
}