package com.example.api.controller

import com.example.api.helper.ResponseMessage
import com.example.api.model.Property
import com.example.api.service.PropertyService
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/api")
class PropertyController(private val propertyService: PropertyService) {

    @PostMapping("/save")
    fun saveProperty(@RequestBody property: Property): ResponseEntity<Property> {
        return ResponseEntity.ok(propertyService.saveProperty(property))
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('Admin')")
    fun updateProperty(@RequestBody property: Property, request: HttpServletRequest,
                       @PathVariable("id") id: Int): ResponseEntity<ResponseMessage> {

        return ResponseEntity.ok(propertyService.updateProperty(request, property, id))
    }

    @GetMapping("/{id}")
    fun getPropertyById(@PathVariable("id") id:Int): ResponseEntity<Property> {
        return ResponseEntity.ok(propertyService.getPropertyById(id))
    }

    @GetMapping("/getAll")
    fun getAllProperties(): ResponseEntity<List<Property>> {
        return ResponseEntity.ok(propertyService.getAllProperties())
    }

    @GetMapping("/getPaginatedProperties")
    fun getPaginatedProperties(@RequestParam("page") page: Int,
                               @RequestParam("size") size: Int): ResponseEntity<Page<Property>> {
        page ?: 0
        size ?: 10
        return ResponseEntity.ok(propertyService.getPaginatedProperties(page, size))
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('Admin')")
    fun deleteProperty(@PathVariable("id") id: Int): String {
        return propertyService.deleteProperty(id)
    }

}