package com.example.api.controller

import com.example.api.model.JwtRequest
import com.example.api.model.JwtResponse
import com.example.api.service.JwtService
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.web.bind.annotation.*

@RestController
class AuthController(private val authenticationManager: AuthenticationManager,
                     private val jwtService: JwtService) {

    @PostMapping("/authenticate")
    @Throws(java.lang.Exception::class)
    fun createJwtToken(@RequestBody jwtRequest: JwtRequest?): JwtResponse? {
        return jwtService.createJwtToken(jwtRequest!!)
    }
}