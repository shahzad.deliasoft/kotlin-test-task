package com.example.api.model

data class JwtResponse (val jwtToken: String)