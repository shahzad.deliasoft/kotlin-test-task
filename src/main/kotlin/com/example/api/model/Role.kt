package com.example.api.model

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "role")
data class Role (
    @Id
    val roleName: String? = null,
    val roleDescription: String? = null
)