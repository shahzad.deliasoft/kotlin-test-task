package com.example.api.model

import javax.persistence.*

@Entity
@Table(name = "coordinates")
data class Coordinates (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id:Int? = null,
    val lat: String? = null,
    val lon: String? = null
)