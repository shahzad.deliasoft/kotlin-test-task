package com.example.api.model

import javax.persistence.*

@Entity
data class User (
    @Id
    val userName: String? = null,
    val userFirstName: String? = null,
    val userLastName: String? = null,
    val userPassword: String? = null,
    @ManyToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinTable(
        name = "USER_ROLE",
        joinColumns = [JoinColumn(name = "USER_ID")],
        inverseJoinColumns = [JoinColumn(name = "ROLE_ID")]
    )
    val role: Set<Role>? = null
)