package com.example.api.model

data class JwtRequest (
                val userName: String,
                val userPassword: String
        )