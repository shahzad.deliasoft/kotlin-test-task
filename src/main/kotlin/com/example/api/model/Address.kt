package com.example.api.model

import javax.persistence.*

@Entity
@Table(name = "address")
data class Address (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,
    val city: String? = null,
    val country: String? = null,
    val zip: String? = null,
    val address: String? = null
)