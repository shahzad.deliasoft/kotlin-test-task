package com.example.api.model

import javax.persistence.*

@Entity
@Table(name = "property")
data class Property (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,
    val roomNumber: Int? = null,
    val square: String? = null,
    val description: String? = null,
    val imageUrl: String? = null,
    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "address_id")
    val address: Address? = null,
    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "coordinates_id")
    val coordinates: Coordinates? = null
)