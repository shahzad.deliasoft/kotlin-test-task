package com.example.api.configuration

import com.example.api.helper.JwtUtil
import com.example.api.service.JwtService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationFilter: OncePerRequestFilter() {

    @Lazy
    @Autowired
    lateinit var customerUserDetailService: JwtService

    @Autowired
    lateinit var jwtUtil: JwtUtil

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        //get request Header
        var requestAuthorizationToken = request.getHeader("Authorization")
        var userName: String = ""
        var jwtToken: String? = null

        if(requestAuthorizationToken != null && requestAuthorizationToken.startsWith("Bearer ")) {
            try {
                jwtToken = requestAuthorizationToken.substring(7)
                userName = jwtUtil.getUsernameFromToken(jwtToken)
            } catch (e: Exception) {

            }
        }
        if (userName != "" && SecurityContextHolder.getContext().authentication == null) {
            val userDetails: UserDetails? = customerUserDetailService.loadUserByUsername(userName)
            if (jwtUtil.validateToken(jwtToken, userDetails)) {
                val usernamePasswordAuthenticationToken =
                    UsernamePasswordAuthenticationToken(userDetails, null, userDetails!!.authorities)
                    usernamePasswordAuthenticationToken.details =
                        WebAuthenticationDetailsSource().buildDetails(request)
                    SecurityContextHolder.getContext().authentication =
                        usernamePasswordAuthenticationToken
                }
        }
        filterChain.doFilter(request,response)
    }
}