package com.example.api.service

import com.example.api.helper.JwtUtil
import com.example.api.model.JwtRequest
import com.example.api.model.JwtResponse
import com.example.api.model.User
import com.example.api.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.*

@Service
class JwtService(private val userRepository: UserRepository,
                 val authenticationManager: AuthenticationManager
): UserDetailsService {

    @Autowired
    lateinit var jwtUtil: JwtUtil

    override fun loadUserByUsername(username: String): UserDetails? {
        val user: User = userRepository.findById(username).get()

        return if (user != null) {
            org.springframework.security.core.userdetails.User(
                user.userName,
                user.userPassword,
                getAuthority(user)
            )
        } else {
            throw UsernameNotFoundException("User not found with username: $username")
        }
    }

    fun createJwtToken(jwtRequest: JwtRequest): JwtResponse? {
        val userName: String = jwtRequest.userName
        val userPassword: String = jwtRequest.userPassword
        authenticate(userName, userPassword)
        val userDetails = loadUserByUsername(userName)
        val newGeneratedToken: String = jwtUtil.generateToken(userDetails)
        //val user: User = userRepository.findById(userName).get()
        return JwtResponse(newGeneratedToken)
    }

    private fun getAuthority(user: User): MutableCollection<out SimpleGrantedAuthority>? {
        val authorities: MutableSet<SimpleGrantedAuthority> = HashSet()
        user.role?.forEach { role -> authorities.add(SimpleGrantedAuthority("ROLE_" + role.roleName)) }
        return authorities
    }

    private fun authenticate(userName: String, userPassword: String) {
        try {
            authenticationManager.authenticate(UsernamePasswordAuthenticationToken(userName, userPassword))
        } catch (e: DisabledException) {
            throw Exception("USER_DISABLED", e)
        } catch (e: BadCredentialsException) {
            throw Exception("INVALID_CREDENTIALS", e)
        }
    }
}