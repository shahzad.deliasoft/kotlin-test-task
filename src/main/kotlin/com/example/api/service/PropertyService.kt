package com.example.api.service

import com.example.api.exception.NotFoundException
import com.example.api.helper.ResponseMessage
import com.example.api.model.Property
import com.example.api.repository.PropertyRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest

@Service
class PropertyService(private val propertyRepository: PropertyRepository) {

    fun saveProperty(property: Property):Property {

        return propertyRepository.save(property);
    }

    fun updateProperty(request: HttpServletRequest, property: Property, id: Int): ResponseMessage {
        var propertyToEdit: Property = getPropertyById(id)
        var responseMessage: ResponseMessage = ResponseMessage(message = "Error, Can't Update", null)
        if (propertyToEdit != null) {
            propertyToEdit = propertyRepository.save(property)
            return ResponseMessage("Success",propertyToEdit)
        }
        return responseMessage
    }

    fun deleteProperty(propertyId: Int): String {
        var propertyToDelete: Property = getPropertyById(propertyId)
        if (propertyToDelete != null) {
            propertyRepository.delete(propertyToDelete)
            return "Property Deleted Successfully"
        }
        return "Property Not Found"
    }

    fun getPropertyById(propertyId: Int):Property {

        return propertyRepository.findById(propertyId)
            .orElseThrow { -> NotFoundException() }
    }

    fun getAllProperties():List<Property> {

        return propertyRepository.findAll()
    }

    fun getPaginatedProperties(page: Int, size: Int): Page<Property> {

        return propertyRepository.findAll(PageRequest.of(page, size))
    }
}