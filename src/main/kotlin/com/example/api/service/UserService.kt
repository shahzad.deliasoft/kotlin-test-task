package com.example.api.service

import com.example.api.model.Role
import com.example.api.model.User
import com.example.api.repository.RoleRepository
import com.example.api.repository.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.HashSet

@Service
class UserService(private val userRepository: UserRepository,
                  private val roleRepository: RoleRepository,
                  private val passwordEncoder: PasswordEncoder) {

    fun initRoleAndUser() {
        val adminRole = Role("Admin","Admin role")
        roleRepository.save(adminRole)

        val userRole = Role("User", "Default role for newly created record")
        roleRepository.save(userRole)

        val adminRoles: MutableSet<Role> = HashSet<Role>()
        adminRoles.add(adminRole)

        val adminUser = User(userName = "admin123",
            userPassword = getEncodedPassword("admin@pass"),
            userFirstName = "admin", userLastName = "admin", role = adminRoles)
        userRepository.save(adminUser)
    }

    fun registerNewUser(user: User): User? {
        val role: Role = roleRepository.findById("User").get()
        val userRoles: MutableSet<Role> = HashSet()
        userRoles.add(role)
        val tempUser = user.copy(role = userRoles, userPassword = getEncodedPassword(user.userPassword))
        return userRepository.save(tempUser)
    }

    fun getEncodedPassword(password: String?): String {
        return passwordEncoder.encode(password)
    }
}