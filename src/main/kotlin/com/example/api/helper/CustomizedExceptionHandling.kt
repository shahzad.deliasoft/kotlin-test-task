package com.example.api.helper

import com.example.api.exception.ExceptionResponse
import com.example.api.exception.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime

@ControllerAdvice
class CustomizedExceptionHandling: ResponseEntityExceptionHandler() {

    @ExceptionHandler(NotFoundException::class)
    fun handleExceptions(exception: NotFoundException?, webRequest: WebRequest?): ResponseEntity<Any>? {
        val response = ExceptionResponse()
        response.dateTime = LocalDateTime.now()
        response.message = "Not found"
        return ResponseEntity(response, HttpStatus.NOT_FOUND)
    }
}