package com.example.api.helper

import com.example.api.model.Property

class ResponseMessage(
    val message: String,
    val data: Property?
)