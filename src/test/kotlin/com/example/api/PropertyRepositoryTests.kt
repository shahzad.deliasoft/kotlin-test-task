package com.example.api

import com.example.api.model.Address
import com.example.api.model.Coordinates
import com.example.api.model.Property
import com.example.api.repository.PropertyRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.Rollback

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class PropertyRepositoryTests {

    @Autowired
    lateinit var propertyRepository: PropertyRepository

    //Junit test for save
    @Test
    @Order(1)
    @Rollback(value = false)
    fun saveProperty() {

        var address: Address = Address(city = "Karachi", country = "Pakistan",
                                       zip = "74510", address = "H no 521")
        var coordinates: Coordinates = Coordinates(lat = "1115015", lon = "44444564")
        var property: Property = Property(roomNumber = 4, square = "50yd", imageUrl = "something",
                                           description = "Luxury", address = address,
                                            coordinates = coordinates)
        propertyRepository.save(property)
        Assertions.assertThat(property.id).isGreaterThan(0)
    }

    @Test
    @Order(2)
    @Rollback(value = false)
    fun getPropertyById() {

        var property: Property = propertyRepository.findById(1).get()
        Assertions.assertThat(property.id).isEqualTo(1)
    }

    @Test
    @Order(3)
    @Rollback(value = false)
    fun getAllProperties() {

        var properties: List<Property> = propertyRepository.findAll()
        Assertions.assertThat(properties.size).isGreaterThan(0)
    }

    @Test
    @Order(4)
    @Rollback(value = false)
    fun updateProperty() {

        var propertyToEdit: Property = propertyRepository.findById(1).get()
        var property: Property = propertyToEdit.copy(description = "Better")
        var updatedProperty = propertyRepository.save(property)
        Assertions.assertThat(updatedProperty.description).isEqualTo("Better")
    }

    @Test
    @Order(5)
    @Rollback(value = false)
    fun deleteProperty() {

        var property = propertyRepository.findById(1).get()
        propertyRepository.delete(property)
        var propertyToCheck: Property = Property()
        var optionalProperty = propertyRepository.findByDescription("Better")
        if (optionalProperty.isPresent) {
            propertyToCheck = optionalProperty.get()
        }
        Assertions.assertThat(propertyToCheck).isNull()
    }
}